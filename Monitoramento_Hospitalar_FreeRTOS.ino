#include <Arduino_FreeRTOS.h>
#include <semphr.h>  // Biblioteca para a utilização de semaforos.
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
    
LiquidCrystal_I2C lcd(0x27 ,16,2);  //I2C PCF8574 no Proteus

// Declara um Mutex Semaphore Handle que usaremos para gerenciar a Porta Serial.
// Ele será usado para garantir que apenas uma tarefa esteja acessando este recurso a qualquer momento.
SemaphoreHandle_t xSerialSemaphore;


// Definição das tres tarefas (uma para os outros sensores, outra para o sensor de batimentos cardiacos e outra para o sensor de oxigenação)
void TaskSensores( void *pvParameters );
void TaskAlarme( void *pvParameters );


// Função setup
void setup() {

   pinMode(13, OUTPUT);   //Definiçao do pino do Led vermelho
   //pinMode(10, OUTPUT);  
   pinMode(12, OUTPUT); //Definiçao do pino do Led verde
   const int pinoBuzzer = 10; //PINO DIGITAL EM QUE O BUZZER ESTÁ CONECTADO
   pinMode(pinoBuzzer, OUTPUT); //DECLARA O PINO COMO SENDO SAÍDA

  // inicializa a Serial
  Serial.begin(9600);

  lcd.begin(16,2); // Inicialização do Display LCD 
  lcd.backlight(); // Liga Backlight do Display
  
  
  
  while (!Serial) {
    ; 
  }

 // Semáforos são úteis para interromper o andamento de uma Tarefa, onde ela deve ser pausada para aguardar,
 // porque está compartilhando um recurso, como a porta serial.
 // Semáforos só devem ser usados enquanto o escalonador estiver rodando, mas podemos configurá-lo aqui.
  if ( xSerialSemaphore == NULL ) // Verifica para confirmar que o Serial Semaphore ainda não foi criado.
  {
    xSerialSemaphore = xSemaphoreCreateMutex();  // Cria um semáforo mutex que usaremos para gerenciar a Porta Serial
    if ( ( xSerialSemaphore ) != NULL )
      xSemaphoreGive( ( xSerialSemaphore ) );  // Disponibiliza a Porta Serial para uso, "Dando" o Semáforo.
  }

  // Criação de duas tarefas que rodarão idependentes
  xTaskCreate(
    TaskSensores
    ,  "TaskSensores"  
    ,  128  
    ,  NULL
    ,  1  // Prioridade
    ,  NULL );

  
  xTaskCreate(
    TaskAlarme
    ,  "TaskAlarme"  
    ,  128  
    ,  NULL
    ,  1  // Prioridade
    ,  0 );   
  
// Agora o Agendador de Tarefas, que assume o controle do agendamento de Tarefas individuais, é iniciado automaticamente.
}

void loop()
{
 // Nada aqui
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void TaskSensores( void *pvParameters __attribute__((unused)) )  // Tarefa 1
{
 
 

  while (1) // Loop da tarefa
  {
    // Leitura dos pinos com a função map para ajustar os valores:
    int Sensores = map (analogRead (A0),0,1023,0,100); 
    int bat = map (analogRead (A1),0,1023,0,300);
    int ox = map (analogRead (A2),0,1023,0,100);
    
    pinMode(12, OUTPUT); 
    
   
     // Se o semáforo não estiver disponível, aguarde 5 ticks do Scheduler para ver se ele fica livre.
    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {
  
     {
   
       lcd.setCursor(1, 0); // coloca o cursor nessa posição
       lcd.print("OS:"); // coloca os caracteres na tela do LCD
       lcd.print(Sensores); // coloca o valor da variável no LCD
       lcd.setCursor(1, 1); // coloca o cursor nessa posição
       lcd.print("BAT:"); // coloca os caracteres na tela do LCD
       lcd.print(bat); // coloca o valor da variável no LCD
       lcd.setCursor(10, 1); // coloca o cursor nessa posição
       lcd.print("O:"); // coloca os caracteres na tela do LCD
       lcd.print(ox); // coloca o valor da variável no LCD
       digitalWrite(12, HIGH); // Acende o LED verde
       
     }
   
      
       
       

      
      xSemaphoreGive( xSerialSemaphore ); // Libera a serial
    }


    vTaskDelay(1);  // delay de 15ms entre as leituras para estabilidade.
  }
}

void TaskAlarme( void *pvParameters __attribute__((unused)) )  // Tarefa 2
{
 
 
  while (1) 
  {
   
    int Sensores = map (analogRead (A0),0,1023,0,100); 
    int bat = map (analogRead (A1),0,1023,0,300);
    int ox = map (analogRead (A2),0,1023,0,100);
    
    pinMode(13, OUTPUT); 
    
  

    if ( xSemaphoreTake( xSerialSemaphore, ( TickType_t ) 5 ) == pdTRUE )
    {
       const int pinoBuzzer = 10; //PINO DIGITAL EM QUE O BUZZER ESTÁ CONECTADO
         pinMode(pinoBuzzer, OUTPUT); //DECLARA O PINO COMO SENDO SAÍDA

      if (bat>120 or bat<60 or ox < 90){ //Condicional do Alarme
  
         digitalWrite(13, HIGH);   //liga o led vermelho
         tone(pinoBuzzer,2000);//ACIONA O BUZZER
         vTaskDelay( 300 / portTICK_PERIOD_MS ); 
        }
        else{
      
        }

        

      
      xSemaphoreGive( xSerialSemaphore ); // Libera a serial
    }


    vTaskDelay(1);  // delay de 15ms entre as leituras para estabilidade.
  }
}
